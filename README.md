Wireshark miniSEED Dissectors
=============================

Dissector plugins for Wireshark to analyze seismic packets sent over UDP.
Work in progress but may already be useful.

Written in Lua but may eventually be ported to C for better performance.

Installation
------------
To add to Wireshark, place the files `miniseed.lua` and/or `q330serv.lua` in one of the following directories:

- **Unix/Linux/macOS:** `~/.config/wireshark/plugins/` or `~/.local/lib/wireshark/plugins/`
- **Windows:** e.g. `C:\Users\your_username\AppData\Roaming\Wireshark\plugins\`

The location may vary. See https://www.wireshark.org/docs/wsug_html/#ChConfigurationPluginFolders

Notes
-----
Both plugins register a heuristic dissector to automatically detect their protocols.
There are no user-configurable preferences yet.

`q330serv.lua` currently only detects packets with the "CI" network code; this will be improved.

Both dissectors register some common protocol fields, so they can be filtered or added to
columns in conjunction with each other:

- seismo.network
- seismo.station
- seismo.channel
- seismo.location
- seismo.sample_rate
- seismo.time


