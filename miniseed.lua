--[[
script-name: miniseed.lua
version: 0.1.0

author: Christopher Bruton <cpbruton@caltech.edu>
Copyright (c) 2021 California Institute of Technology

Wireshark dissector for raw miniSEED 2.x sent over UDP.
Currently only analyzes the fixed header, not the blockettes or data.
This registers a heuristic identifier to identify regardless of UDP port number.

To use, place this file in one of the following directories (probably):

  Unix/Linux/macOS: ~/.config/wireshark/plugins/
                or: ~/.local/lib/wireshark/plugins/

  Windows:     e.g. C:\Users\your_username\AppData\Roaming\Wireshark\plugins\

The location may vary. See https://www.wireshark.org/docs/wsug_html/#ChConfigurationPluginFolders

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

--]]

local miniseed = Proto("miniseed", "miniSEED Protocol")


-- Header Fields
local pf_sequence_number        = ProtoField.string       ("miniseed.sequence_number",        "Sequence Number")
local pf_data_quality           = ProtoField.char         ("miniseed.data_quality",           "Data Quality")
local pf_reserved               = ProtoField.char         ("miniseed.reserved",               "Reserved")
local pf_station                = ProtoField.string       ("seismo.station",                "Station")
local pf_location               = ProtoField.string       ("seismo.location",               "Location")
local pf_channel                = ProtoField.string       ("seismo.channel",                "Channel")
local pf_network                = ProtoField.string       ("seismo.network",                "Network")
--local pf_time                   = ProtoField.absolute_time("miniseed.time",                   "Time", base.UTC)
local pf_time                   = ProtoField.string       ("seismo.time",                   "Data Time")
local pf_time_year              = ProtoField.uint16       ("miniseed.time.year",              "Year")
local pf_time_day               = ProtoField.uint16       ("miniseed.time.day",               "Day")
local pf_time_hour              = ProtoField.uint8        ("miniseed.time.hour",              "Hour")
local pf_time_minute            = ProtoField.uint8        ("miniseed.time.minute",            "Minute")
local pf_time_second            = ProtoField.uint8        ("miniseed.time.second",            "Second")
local pf_time_unused            = ProtoField.uint8        ("miniseed.time.unused",            "Unused")
local pf_time_fraction          = ProtoField.uint16       ("miniseed.time.fraction",          "Fraction")
local pf_num_samples            = ProtoField.uint16       ("miniseed.num_samples",            "Number of Samples")
local pf_sample_rate_factor     = ProtoField.uint16       ("seismo.sample_rate",     "Sample Rate Factor")
local pf_sample_rate_multiplier = ProtoField.uint16       ("miniseed.sample_rate_multiplier", "Sample Rate Multiplier")
local pf_activity_flags         = ProtoField.uint8        ("miniseed.activity_flags",         "Activity Flags")
local pf_io_clock_flags         = ProtoField.uint8        ("miniseed.io_clock_flags",         "I/O and Clock Flags")
local pf_data_quality_flags     = ProtoField.uint8        ("miniseed.data_quality_flags",     "Data Quality Flags")
local pf_num_blockettes         = ProtoField.uint8        ("miniseed.num_blockettes",         "Number of Blockettes")
local pf_time_correction        = ProtoField.int32        ("miniseed.time_correction",        "Time Correction")
local pf_data_offset            = ProtoField.uint16       ("miniseed.data_offset",            "Data Offset")
local pf_blockette_offset       = ProtoField.uint16       ("miniseed.blockette_offset",       "Blockette Offset")




-- All bytes after the header; these will be miniSEED blockettes and/or seismic data.
-- We haven't written code to dissect these yet so we display as raw unsigned bytes.
local pf_payload = ProtoField.ubytes("miniseed.payload", "Payload Bytes")


-- Register the fields
miniseed.fields = {
    pf_sequence_number,
    pf_data_quality,
    pf_reserved,
    pf_station,
    pf_location,
    pf_channel,
    pf_network,
    pf_time,
    pf_time_year,
    pf_time_day,
    pf_time_hour,
    pf_time_minute,
    pf_time_second,
    pf_time_unused,
    pf_time_fraction,
    pf_num_samples,
    pf_sample_rate_factor,
    pf_sample_rate_multiplier,
    pf_activity_flags,
    pf_io_clock_flags,
    pf_data_quality_flags,
    pf_num_blockettes,
    pf_time_correction,
    pf_data_offset,
    pf_blockette_offset
}

-- Add expert fields
local ef_too_short = ProtoExpert.new("miniseed.too_short.expert", "miniSEED record too short.",
                                     expert.group.MALFORMED, expert.severity.ERROR)

-- Register the experts
miniseed.experts = {
    ef_too_short
}

-- miniSEED header length (minimum packet data length)
local MS_HDR_LEN = 48

-- This is the actual dissector function
function miniseed.dissector(tvbuf, pktinfo, root)
    -- Set the protocol column to display protocol name
    pktinfo.cols.protocol:set("miniSEED")

    -- Get length of buffer
    local pktlen = tvbuf:reported_length_remaining()

    -- Add to dissection display tree
    local tree = root:add(miniseed, tvbuf:range(0,pktlen))

    -- Check that the length is at least the correct header length
    if pktlen < MS_HDR_LEN then
        tree:add_proto_expert_info(ef_too_short)
        return
    end

    -- Start adding the fields to the tree

    -- Sequence number
    tree:add(pf_sequence_number, tvbuf:range(0,6))
    -- Data quality
    tree:add(pf_data_quality, tvbuf:range(6,1))
    -- Reserved byte
    tree:add(pf_reserved, tvbuf:range(7,1))
    -- Station
    tree:add(pf_station, tvbuf:range(8,5))
    -- Location
    tree:add(pf_location, tvbuf:range(13,2))
    -- Channel
    tree:add(pf_channel, tvbuf:range(15,3))
    -- Network code
    tree:add(pf_network, tvbuf:range(18,2))

    -- Next is the time
    local year = tvbuf:range(20,2):uint()
    local day  = tvbuf:range(22,2):uint()
    local hour = tvbuf:range(24,1):uint()
    local min  = tvbuf:range(25,1):uint()
    local sec  = tvbuf:range(26,1):uint()
    -- local unused = tvbuf:range(27,1):uint()
    local frac = tvbuf:range(28,2):uint()

    local time_str = string.format("%04d-%03d %02d:%02d:%02d.%04d", year, day, hour, min, sec, frac)

    local time_tree = tree:add(pf_time, tvbuf:range(20,10), time_str)

    time_tree:add(pf_time_year, tvbuf:range(20,2))
    time_tree:add(pf_time_day,  tvbuf:range(22,2))
    time_tree:add(pf_time_hour, tvbuf:range(24,1))
    time_tree:add(pf_time_minute, tvbuf:range(25,1))
    time_tree:add(pf_time_second, tvbuf:range(26,1))
    time_tree:add(pf_time_unused, tvbuf:range(27,1))
    time_tree:add(pf_time_fraction, tvbuf:range(28,2))

    -- Number of samples
    tree:add(pf_num_samples, tvbuf:range(30,2))
    -- Sample rate factor
    tree:add(pf_sample_rate_factor, tvbuf:range(32,2))
    -- Sample rate multiplier
    tree:add(pf_sample_rate_multiplier, tvbuf:range(34,2))

    -- Todo: break down the flags properly
    -- Activity flags
    tree:add(pf_activity_flags, tvbuf:range(36,1))
    -- I/O and clock flags
    tree:add(pf_io_clock_flags, tvbuf:range(37,1))
    -- Data quality flags
    tree:add(pf_data_quality_flags, tvbuf:range(38,1))

    -- Number of blockettes that follow
    tree:add(pf_num_blockettes, tvbuf:range(39,1))

    -- Time Correction
    tree:add(pf_time_correction, tvbuf:range(40,4))

    -- Data offset
    tree:add(pf_data_offset, tvbuf:range(44,2))

    -- Blockette offset
    tree:add(pf_blockette_offset, tvbuf:range(46,2))

    -- Return the header length for now
    return MS_HDR_LEN
end

-- DissectorTable.get("udp.port"):add(8495, miniseed)


-- Heuristic dissector

local function heur_dissect_miniseed(tvbuf, pktinfo, root)

    -- Check the length
    if tvbuf:len() < MS_HDR_LEN then
        return false
    end

    -- Check first six bytes - must be an ascii number
    local n = tonumber(tvbuf:range(0,6):string(ENC_ASCII))
    if n == nil then
        return false
    end

    -- The next character must be D, R, Q, or M
    local d = tvbuf:range(6,1):string(ENC_ASCII)
    if d ~= "D" and d ~= "R" and d ~= "Q" and d ~= "M" then
        return false
    end

    -- The next character must be a space
    if tvbuf:range(7,1):string(ENC_ASCII) ~= " " then
        return false
    end

    -- The next 10 characters must be uppercase letters, numbers, or space
    if string.match(tvbuf:range(8,10):string(ENC_ASCII), "[A-Z0-9 ]") == nil then
        return false
    end

    -- The next two characters (network code) must be upper or lowercase letters or numbers
    if string.match(tvbuf:range(10,2):string(ENC_ASCII), "[A-Za-z0-9]") == nil then
        return false
    end

    -- This is good enough for testing anyway. Now run the real dissector.

    miniseed.dissector(tvbuf, pktinfo, root)

    -- Set conversation
    -- pktinfo.conversation = miniseed

    return true
end

-- Register the Heuristic

miniseed:register_heuristic("udp", heur_dissect_miniseed)








